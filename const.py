"""Constants for the Solarmax integration."""
from __future__ import annotations

from collections.abc import Callable
from dataclasses import dataclass
from datetime import datetime

from homeassistant.components.sensor import (
    SensorDeviceClass,
    SensorEntityDescription,
    SensorStateClass,
)
from homeassistant.const import (
    ELECTRIC_POTENTIAL_VOLT,
    ENERGY_KILO_WATT_HOUR,
    PERCENTAGE,
    POWER_WATT,
)
from homeassistant.util.dt import as_local

DOMAIN = "solarmax"
DEFAULT_NAME = "Solarmax"

@dataclass
class SorlarmaxSensorEntryDescription(SensorEntityDescription):
    """Describes Solarmax sensor entry."""

    value: Callable[[int | float], float] | Callable[[datetime], datetime] | None = None

SENSOR_TYPES: tuple[SorlarmaxSensorEntryDescription, ...] = (
    SorlarmaxSensorEntryDescription(
        key="time",
        name="last update",
        device_class=SensorDeviceClass.TIMESTAMP,
        value=as_local,
    ),
    SorlarmaxSensorEntryDescription(
        key="energy_total",
        name="Energy total",
        native_unit_of_measurement=ENERGY_KILO_WATT_HOUR,
        device_class=SensorDeviceClass.ENERGY,
        state_class=SensorStateClass.TOTAL,
    ),
)
