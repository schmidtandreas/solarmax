"""Platform for sensor integration."""
from homeassistant.core import HomeAssistant
from homeassistant.helpers.entity_platform import AddEntitiesCallback
from homeassistant.helpers.typing import ConfigType, DiscoveryInfoType
from homeassistant.components.sensor import SensorEntity

from .const import DOMAIN, DEFAULT_NAME, SENSOR_TYPES, SorlarmaxSensorEntryDescription


def setup_platform(
    hass: HomeAssistant,
    config: ConfigType,
    add_entities: AddEntitiesCallback,
    discovery_info: DiscoveryInfoType | None = None
) -> None:
    """Set up the sensor platform."""
    add_entities(SolarmaxSensor(description) for description in SENSOR_TYPES)


class SolarmaxSensor(SensorEntity):
    """Representation of the Solarmax Sensor."""

    def __init__(self, description: SorlarmaxSensorEntryDescription) -> None:
        self.entity_description = description
        self._attr_name = f"{DEFAULT_NAME} {description.name}"
        self._attr_unique_id = f"{DOMAIN}_{description.key}"
        self._attr_native_unit_of_measurement = description.native_unit_of_measurement
        self._attr_device_class = description.device_class
        if description.state_class:
            self._attr_state_class = description.state_class
        self._my_value = 0

    def update(self) -> None:
        """Fetch new state data for the sensor.
        This is the only method that should fetch new data for Home Assistant.
        """
        #TODO Use maxtalk protocal to get the current value from Solarmax
        if self._attr_name != "{DEFAULT_NAME} last update":
            self._attr_native_value = self._my_value
            self._my_value += 1
        else:
            self._attr_native_value = description.value
